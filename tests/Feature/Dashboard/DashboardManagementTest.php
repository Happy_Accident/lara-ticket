<?php

namespace Tests\Feature\Dashboard;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DashboardManagementTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function agent_dashboard_can_be_reached()
    {
        $response = $this->get('/dashboard');
        $response->assertStatus(200);
    }
}
