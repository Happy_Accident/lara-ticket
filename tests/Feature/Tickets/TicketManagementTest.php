<?php

namespace Tests\Feature\Tickets;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TicketManagementTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function agent_ticket_screen_can_be_reached()
    {
        $response = $this->get('/tickets');

        $response->assertStatus(200);
    }
}
