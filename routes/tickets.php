<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;

Route::get('/tickets', function(){
    return Inertia::render('Ticket/Tickets');
})->middleware(['auth', 'verified'])->name('tickets');
