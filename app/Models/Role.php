<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public const ADMINISTRATOR = 1;
    public const MANAGER = 2;
    public const AGENT = 3;
    public const USER = 4;
}
