<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    use HasFactory;

    public const LOW = 1;
    public const MEDIUM = 2;
    public const HIGH = 3;
    public const URGENT = 3;
}
