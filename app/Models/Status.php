<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    public const OPEN = 1;
    public const PENDING = 2;
    public const CLOSED = 3;
    public const RESOLVED = 4;

}
