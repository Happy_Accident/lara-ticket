<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'id' => 1,
            'status' => 'Open'
        ]);
        DB::table('statuses')->insert([
            'id' => 2,
            'status' => 'Pending'
        ]);
        DB::table('statuses')->insert([
            'id' => 3,
            'status' => 'Closed'
        ]);
        DB::table('statuses')->insert([
            'id' => 4,
            'status' => 'Resolved'
        ]);
    }
}
