<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Constraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
            $table->foreign('group')->nullable()->references('id')->on('groups');
            $table->foreign('role')->nullable()->references('id')->on('roles');
        });

        Schema::table('tickets', function (Blueprint $table){
           $table->foreign('customer')->references('id')->on('customers');
           $table->foreign('agent')->references('id')->on('users');
           $table->foreign('status')->references('id')->on('statuses');
           $table->foreign('priority')->references('id')->on('priorities');
        });

        Schema::table('customers', function (Blueprint $table){
           $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('ticket_conversations', function (Blueprint $table){
            $table->foreign('ticket_id')->references('id')->on('tickets');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table){
            $table->dropForeign(['group']);
            $table->dropForeign(['role']);
        });
        Schema::table('tickets', function (Blueprint $table){
            $table->dropForeign(['customer']);
            $table->dropForeign(['agent']);
            $table->dropForeign(['status']);
            $table->dropForeign(['priority']);
        });
        Schema::table('customers', function (Blueprint $table){
            $table->dropForeign(['company_id']);
        });
        Schema::table('ticket_conversations', function (Blueprint $table){
            $table->dropForeign(['ticket_id']);
        });
    }
}
