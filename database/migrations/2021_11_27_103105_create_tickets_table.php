<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('customer')->unsigned();
            $table->bigInteger('agent')->unsigned()->nullable();
            $table->string('subject');
            $table->text('body');
            $table->text('body_raw');
            $table->bigInteger('status')->unsigned()->default(\App\Models\Status::OPEN);
            $table->dateTime('due_by');
            $table->bigInteger('priority')->unsigned()->default(\App\Models\Priority::LOW);
            $table->enum('tags', [NULL]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
